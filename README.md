# Introduction
A wine aromas wheel via Latex.

# Prerequisites
Just install the full version of texlive :)
for ubuntu users:

	$ apt-get install texlive-full

# How to build

	$ pdflatex wine_wheel.tex  

Then you will find a pdf file in the same folder.

# To contribute
Please feel free to contribute. 
Contact: zhe@mines-albi.fr

